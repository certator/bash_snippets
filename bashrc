CDIR=`pwd`

#alias ll='ls -lG'

case $(uname) in
       Linux)
       alias ll='ls --color=auto -alF'
       ;;
       Darwin)
	alias ll='ls -lG'
	alias eog='qlmanage -p'
	export CLICOLOR=cons25
       ;;
esac

alias gitpp="git push origin \`git symbolic-ref -q HEAD\`"
alias gitpl="git pull origin \`git symbolic-ref -q HEAD\`"

function findgrep {
	find . -name $1 -exec grep -Hn $2 {} \;
}

#echo "${BASH_SOURCE[0]}"

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ] ; do cd "$( dirname "${SOURCE}" )"; SOURCE="$(readlink "$SOURCE")"; done
DIR="$( cd "$( dirname "${SOURCE}" )" && pwd )"

cd "$CDIR"

[ -e ~/.bashrc_old ] && . ~/.bashrc_old

. $DIR/git_promt1

function stitle
{
	echo -n `date`
}

function killalls 
{
	r=`ps -ef`; pids=`echo "$r" | grep "$1" | awk '{print $2}'`
	kill $2 $pids
#	echo $pids
}

#cd $CDIR


#PS1="$(stitle) ${TITLEBAR} > "
